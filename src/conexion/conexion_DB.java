/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Islena
 */
public class conexion_DB {
    
    public static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    public static final String BASE = "prueba_login";
    public static final String URL = "jdbc:mysql://localhost:3306/"+BASE+"?useTimezone=true&serverTimezone=UTC";
    public static final String USERNAME = "root";
    public static final String PASSWORD = "ADMIN";
     
    //metodo coexion
    public static Connection getConection(){
       
        Connection  con = null;
        
        try{
            Class.forName(DRIVER);
            con = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            
            System.out.println("Conexion Exitosa");
            
        }catch (Exception e){
            
            System.out.println(e + "   Fallo conexion");
            
        }
        return con;
    }
    
}
