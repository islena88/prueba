/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frame;



import static conexion.conexion_DB.getConection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Islena
 */
public class login extends javax.swing.JFrame {

    Connection con= getConection();
    
    public void validaAcceso(){
    
    int resultado=0;
    
    try{
        String usu =new String(txtUsuario.getText());
        String cont =new String(txtContrasena.getPassword());
        System.out.println(usu);
        System.out.println(cont);
        String sql="select * from usuario_login where nombre_usuario = '" + usu +"' and pass_usuario ='" + cont +"' ";
        
        Statement st=con.createStatement(); 
        ResultSet rs=st.executeQuery(sql);
        
        if(rs.next()){
                
                    resultado=1;

                    if(resultado == 1 ){
                        lista_producto LP = new lista_producto();
                        LP.setVisible(true);
                        dispose();
                    } 
            
        } /*else {       
                     JOptionPane.showMessageDialog(this, "Usuario / Contraseña no valido ");
                     
                }*/
        
    } catch (Exception e){
        
            JOptionPane.showMessageDialog(this, "Error al acceder vuelva a intentar  " + e.getMessage());
    }
    
    
    
    }
    
    public login() {
        initComponents();
        this.setLocationRelativeTo(null);
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        login = new javax.swing.JPanel();
        lblContrasena = new javax.swing.JLabel();
        lblUsuario = new javax.swing.JLabel();
        icoUsuario = new javax.swing.JLabel();
        txtUsuario = new javax.swing.JTextField();
        jSeparatorUsuario = new javax.swing.JSeparator();
        txtContrasena = new javax.swing.JPasswordField();
        jSeparatorContrasena = new javax.swing.JSeparator();
        btnIngresar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Login");
        setAlwaysOnTop(true);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setResizable(false);

        login.setBackground(new java.awt.Color(255, 255, 255));
        login.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        login.setPreferredSize(new java.awt.Dimension(480, 420));
        login.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblContrasena.setBackground(new java.awt.Color(108, 61, 183));
        lblContrasena.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        lblContrasena.setForeground(new java.awt.Color(108, 61, 183));
        lblContrasena.setText("Contraseña");
        login.add(lblContrasena, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 200, 110, -1));

        lblUsuario.setBackground(new java.awt.Color(108, 61, 183));
        lblUsuario.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        lblUsuario.setForeground(new java.awt.Color(108, 61, 183));
        lblUsuario.setText("Usuario");
        login.add(lblUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 150, 90, -1));

        icoUsuario.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        icoUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/person_ico.png"))); // NOI18N
        icoUsuario.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        login.add(icoUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, 130, 120));

        txtUsuario.setForeground(new java.awt.Color(153, 153, 153));
        txtUsuario.setBorder(null);
        txtUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUsuarioActionPerformed(evt);
            }
        });
        login.add(txtUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 170, 100, -1));
        login.add(jSeparatorUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 190, 110, 20));

        txtContrasena.setForeground(new java.awt.Color(153, 153, 153));
        txtContrasena.setBorder(null);
        txtContrasena.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtContrasenaActionPerformed(evt);
            }
        });
        login.add(txtContrasena, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 220, 110, -1));
        login.add(jSeparatorContrasena, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 240, 110, 20));

        btnIngresar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/boton-01.png"))); // NOI18N
        btnIngresar.setBorderPainted(false);
        btnIngresar.setContentAreaFilled(false);
        btnIngresar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnIngresar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/image/boton-02.png"))); // NOI18N
        btnIngresar.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/image/boton-02.png"))); // NOI18N
        btnIngresar.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/image/boton-02.png"))); // NOI18N
        btnIngresar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnIngresarMouseClicked(evt);
            }
        });
        btnIngresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIngresarActionPerformed(evt);
            }
        });
        login.add(btnIngresar, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 250, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addComponent(login, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(60, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(login, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUsuarioActionPerformed
        // TODO add your handling code here: aqui es input user
    }//GEN-LAST:event_txtUsuarioActionPerformed

    private void txtContrasenaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtContrasenaActionPerformed
        // TODO add your handling code here: aqui es input password
    }//GEN-LAST:event_txtContrasenaActionPerformed

    private void btnIngresarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnIngresarMouseClicked
        // TODO add your handling code here:
        validaAcceso();
        
        
    }//GEN-LAST:event_btnIngresarMouseClicked

    private void btnIngresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIngresarActionPerformed
         
    }//GEN-LAST:event_btnIngresarActionPerformed

   
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnIngresar;
    private javax.swing.JLabel icoUsuario;
    private javax.swing.JSeparator jSeparatorContrasena;
    private javax.swing.JSeparator jSeparatorUsuario;
    private javax.swing.JLabel lblContrasena;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JPanel login;
    private javax.swing.JPasswordField txtContrasena;
    private javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables
}
